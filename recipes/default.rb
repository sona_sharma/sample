#
# Cookbook:: mynginx
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
package 'nginx' do
  action :install
end

package 'apache2' do
  action :remove
end

service 'nginx' do
  action [ :enable, :start ]
end
